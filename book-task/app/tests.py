#from django.test import TestCase
import json
from django.urls import include, path, reverse
from rest_framework.test import APITestCase
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.test import APIRequestFactory

from app.serializer import AuthorSerializer

from .models import AuthorModel
from .models import BookAuthorModel
from .models import BooksModel
from app import views 
#from django.test.utils import setup_test_environment
class AuthorTests(APITestCase):
    def setUp(self):
        AuthorModel.objects.create(average_rating=3.5,author_id=8443193,text_reviews_count=2,name="Anisa Febri",ratings_count=6)
        AuthorModel.objects.create(average_rating=3.5,author_id=8443194,text_reviews_count=2,name="Chacil",ratings_count=6)
        AuthorModel.objects.create(average_rating=3.88,author_id=8443195,text_reviews_count=2,name="Diva Apresya",ratings_count=8)
        AuthorModel.objects.create(average_rating=3.5,author_id=8443196,text_reviews_count=2,name="Hanna Kirani",ratings_count=6)
        AuthorModel.objects.create(average_rating=3.36,author_id=8443172,text_reviews_count=4,name="Katarina Sora",ratings_count=11)
                
        
    def test_author(self):  
        url='/api/author/'
        response = self.client.get(url,format='json') 
        #print(response.data)       
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'],5)
    
    def test_author_limit(self):  
        url='/api/author/?page_size=3'
        response = self.client.get(url,format='json')          
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']),3) 
        
  
    def test_author_name(self):
         url='/api/author/?filter_type=name&value=Diva'
         response = self.client.get(url, format='json')  
         self.assertEqual(response.status_code, status.HTTP_200_OK)
         self.assertEqual(response.data['count'],1)
         #print(response.data)      
         
    def test_author_name_containing(self):
         url='/api/author/?filter_type=name_containing&value=anna'
         response = self.client.get(url, format='json')  
         self.assertEqual(response.status_code, status.HTTP_200_OK)
         self.assertEqual(response.data['count'],1) 
         #print(response.data)   
         
class BooksTests(APITestCase):
    def setUp(self):
        BooksModel.objects.create(book_id=27852981,title="Sound Explosion!",title_without_series="Sound Explosion!",publisher="Wrecking Crew LLC",description="The inside story of The Wrecking Crew, who provided the instrumental backing for some of the most timeless and recognizable songs of the '60s and early '70s. They were an elite group of Los Angeles based studio musicians whose roster of hit songs served as the soundtrack for a generation.")
        BooksModel.objects.create(book_id=16127694,title="Sound Bites",title_without_series="Sound Bites",publisher="Resource Publications (OR)",description="The absurdities of contemporary politics and culture are lampooned in this biting novel, composed entirely of media \"sound bites.\" Politicians, reporters, pundits, even voters, are skewered in an all-too-believable story about a Senatorial campaign between a young conservative woman and a venerable liberal man. The result is a unique, fast-paced satire filled with sharp dialogue and ironic surprises.")
        BooksModel.objects.create(book_id=25132487,title="Sound & Fury: Shakespeare Goes Punk",title_without_series="Sound & Fury",publisher="Writerpunk Press",description="Airships and sky pirates! Brain Modification chips! Technologically enhanced nymphs! Shakespeare goes punk in this first volume of stories from Writerpunk Press.\nProfits to go to PAWS Lynwood, an animal shelter and wildlife rescue.\nAsk a bunch of eclectic writers to write stories inspired by one of the greatest dramatists of all time. Cast the stories in various punk genres: Cyber, Tesla, Diesel, Steam, Clock. Result: an innovative collection of stories inspired by the Bard, with a twist! Punk stories show the path not taken or the path that shouldn't be taken. Let us reshape your world.")
        BooksModel.objects.create(book_id=16688430,title="Sounding the Seasons: Seventy Sonnets for the Christian Year",title_without_series="Sounding the Seasons: Seventy Sonnets for the Christian Year",publisher="Canterbury Press",description="Poetry has always been a central element of Christian spirituality and is increasingly used in worship, in pastoral services and guided meditation. In Sounding the Seasons, Cambridge poet, priest and singer-songwriter Malcolm Guite transforms seventy lectionary readings into lucid, inspiring poems, for use in regular worship, seasonal services, meditative reading or on retreat. Already widely recognised, Malcolm's writing has been acclaimed by Rowan Williams and Luci Shaw, two leading contemporary religious poets. Seven Advent poems from this collection will appear in the next edition of Penguin's (US) Best Spiritual Writing edited by Philip Zaleski, alongside the work of writers such as Seamus Heaney and Annie Dillard. Malcolm Guite is Chaplain of Girton College, Cambridge. A performance poet and singer/songwriter, he lectures widely on poetry and theology in Britain and the US and has a large following on his website, www.malcolmguite.wordpress.com. He is a contributor to Reflections for Daily Prayer.")
        BooksModel.objects.create(book_id=2166989,title="Sounds Great 1: Low Intermediate Pronunciation for Speakers of English",title_without_series="Sounds Great 1: Low Intermediate Pronunciation for Speakers of English",publisher="Heinle ELT",description="SOUNDS GREAT teaches learners to discover, recognize, and use American English word stress, sentence stress, intonation patterns, and high-frequency vowels and consonants. The many guided conversations, pair and small group practices, information gap activities, peer interviews, and short oral reports invite students to enjoy improving their command of spoken English")
        #BooksModel.objects.create(book_id=,title="",title_without_series="",publisher="",description="")    
        
        AuthorModel.objects.create(average_rating=3.5,author_id=46591,text_reviews_count=2,name="Ken Sharp",ratings_count=6)
        AuthorModel.objects.create(average_rating=3.5,author_id=1183,text_reviews_count=2,name="Victor L. Cahn",ratings_count=6)
        AuthorModel.objects.create(average_rating=3.5,author_id=13631154,text_reviews_count=2,name="H. James Lopez",ratings_count=6)
        AuthorModel.objects.create(average_rating=3.5,author_id=943290,text_reviews_count=2,name="Malcolm Guite",ratings_count=6)
        AuthorModel.objects.create(average_rating=3.5,author_id=983982,text_reviews_count=2,name="Beverly Beisbier",ratings_count=6)
        
        BookAuthorModel.objects.create(author_id=46591,book_id=27852981)
        BookAuthorModel.objects.create(author_id=1183,book_id=16127694)
        BookAuthorModel.objects.create(author_id=13631154,book_id=25132487)
        BookAuthorModel.objects.create(author_id=943290,book_id=16688430)
        BookAuthorModel.objects.create(author_id=983982,book_id=2166989)
        
        
        
    def test_book(self):
        url='/api/books/'
        response = self.client.get(url,format='json') 
        #print(response.data)       
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'],5)
        
    def test_book_limit(self):
        url='/api/books/?page_size=3'
        response = self.client.get(url,format='json') 
        #print(response.data)       
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']),3)
        
    def test_book_title(self):
        url='/api/books/?filter_type=title&value=sounding'
        response = self.client.get(url,format='json') 
        #print(response.data)       
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'],1)
        
    def test_book_title_containing(self):
        url='/api/books/?filter_type=title_containing&value=Great'
        response = self.client.get(url,format='json') 
        #print(response.data)       
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'],1)

    def test_book_author_name(self):
        url='/api/books/?filter_type=author_name&value=Ken'
        response = self.client.get(url,format='json') 
        #print(response.data)   
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'],1)
        
    def test_book_author_name_containing(self):
        url='/api/books/?filter_type=author_name_containing&value=Guite'
        response = self.client.get(url,format='json') 
        #print(response.data)   
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'],1)
