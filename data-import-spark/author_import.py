from pyspark.sql.functions import *
from pyspark.sql import SparkSession
from pyspark.sql.types import IntegerType
spark = SparkSession \
    .builder \
    .appName('Python Spark Postgresql') \
    .getOrCreate()
    
rawDF = spark.read.json("./authors.json")
rawDF.registerTempTable("authors")
output=rawDF.select(rawDF.author_id.cast("long"),rawDF.average_rating.cast("double"),rawDF.text_reviews_count.cast("int"),rawDF.ratings_count.cast("int"),rawDF.name)
output.write.format('jdbc').options(
        url='jdbc:postgresql://localhost:5432/model',
        dbtable='authors',
        user='postgres',
        password='pass').mode('append').save() 
