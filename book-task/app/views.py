from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import AuthorModel
from .models import BookAuthorModel
from .models import BooksModel
from app.serializer import AuthorSerializer
from app.serializer import BookAuthorSerializer
from app.serializer import BooksSerializer
from django.core import serializers
from rest_framework import generics
from rest_framework import pagination


class Author(generics.ListCreateAPIView):
    serializer_class = AuthorSerializer
    def get_queryset(self):
        filter_type = self.request.GET.get('filter_type', " ")
        value = self.request.GET.get('value', " ")
        pagination.PageNumberPagination.page_size = self.request.GET.get('page_size', "10")

        if(filter_type == 'name'):
            return AuthorModel.objects.filter(name__istartswith=value)
            
        elif(filter_type == 'name_containing'):
            return AuthorModel.objects.filter(name__contains=value)
            
        else:
            return AuthorModel.objects.all()
            

    
class BookAuthor(generics.ListCreateAPIView):
    
    def get_serializer_class(self):
        if self.request.GET.get('filter_type') == 'author_name' or self.request.GET.get('filter_type') == 'author_name_containing' :
            return BookAuthorSerializer
        return BooksSerializer

    def get_queryset(self):
        filter_type = self.request.GET.get('filter_type', " ")
        value = self.request.GET.get('value', " ")
        pagination.PageNumberPagination.page_size = self.request.GET.get('page_size', "10")
        
        if(filter_type == 'title'):
            return BooksModel.objects.filter(title__istartswith=value)


        elif(filter_type == 'title_containing'):
            return BooksModel.objects.filter(title__contains=value)

        elif(filter_type == 'author_name'):
            q1 = AuthorModel.objects.filter(name__istartswith=value)
            return BookAuthorModel.objects.filter(author_id__in=q1)


        elif(filter_type == 'author_name_containing'):
            q1 = AuthorModel.objects.filter(name__contains=value)
            return BookAuthorModel.objects.filter(author_id__in=q1)
            
        else:
            return BooksModel.objects.all()

