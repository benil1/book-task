from pyspark.sql.functions import *
from pyspark.sql import SparkSession
from pyspark.sql.types import IntegerType
spark = SparkSession \
    .builder \
    .appName('Python Spark Postgresql') \
    .getOrCreate()
    
rawDF = spark.read.json("./books.json")
rawDF.registerTempTable("books")

output=rawDF.select(rawDF.book_id.cast("long"),rawDF.title,rawDF.title_without_series,rawDF.description,rawDF.publisher)
output.write.format('jdbc').options(
        url='jdbc:postgresql://localhost:5432/model',
        dbtable='books',
        user='postgres',
        password='pass').mode('append').save()
