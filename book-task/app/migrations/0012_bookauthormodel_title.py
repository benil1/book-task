# Generated by Django 3.2.5 on 2021-07-22 13:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0011_auto_20210722_1655'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookauthormodel',
            name='title',
            field=models.TextField(null=True),
        ),
    ]
