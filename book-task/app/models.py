from django.db import models
from django.contrib.postgres.fields import ArrayField
# Create your models here.
        


class AuthorModel(models.Model):
    average_rating=models.FloatField(null=True)
    author_id=models.BigIntegerField(primary_key=True)
    text_reviews_count=models.IntegerField(null=True)
    name=models.TextField(null=True)
    ratings_count=models.IntegerField(null=True)
    class Meta:
        db_table = 'authors' 
        
class BooksModel(models.Model):
    description=models.TextField(null=True)
    publisher=models.TextField(null=True)
    book_id=models.BigIntegerField(primary_key=True)
    title=models.TextField(null=True)
    title_without_series=models.TextField(null=True)
    class Meta:
        db_table='books'
        
        
        
class BookAuthorModel(models.Model):
    author=models.ForeignKey(AuthorModel,on_delete=models.CASCADE,null=True)
    book=models.ForeignKey(BooksModel,on_delete=models.CASCADE,null=True)    
    class Meta: 
        db_table='book_author'
        
        
""" class BooksModel(models.Model):
    #isbn=models.TextField(null=True)
    #text_reviews_count=models.TextField(null=True)
    #series=ArrayField(models.TextField(null=True),null=True)
    #country_code=models.TextField(null=True)
    #language_code=models.TextField(null=True)
    #asin=models.TextField(null=True)
    #is_ebook=models.TextField(null=True)
    #average_rating=models.TextField(null=True)
    #kindle_asin=models.TextField(null=True)
    #similar_books=ArrayField(models.TextField(null=True),null=True)
    description=models.TextField(null=True)
    #format=models.TextField(null=True)
    #link=models.TextField(null=True)
    publisher=models.TextField(null=True)
    #num_pages=models.TextField(null=True)
    #publication_day=models.TextField(null=True)
    #isbn13=models.TextField(null=True)
    #publication_month=models.TextField(null=True)
    #edition_information=models.TextField(null=True)
    #publication_year=models.TextField(null=True)
    #url=models.TextField(null=True)
    #image_url=models.TextField(null=True)
    book_id=models.BigIntegerField(primary_key=True)
    #ratings_count=models.TextField(null=True)
    #work_id=models.TextField(null=True)
    title=models.TextField(null=True)
    title_without_series=models.TextField(null=True)
    class Meta:
        db_table='books' """
        

        
