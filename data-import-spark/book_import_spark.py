from pyspark.sql.functions import *
from pyspark.sql import SparkSession
from pyspark.sql.types import IntegerType
spark = SparkSession \
    .builder \
    .appName('Python Spark Postgresql') \
    .getOrCreate()
rawDF = spark.read.json("./books.json")
rawDF.registerTempTable("books")
rawDF=rawDF.select(explode("authors.author_id").alias("author_id"),rawDF.book_id)
output=rawDF.select(rawDF.author_id.cast("long"),rawDF.book_id.cast("long"))
output.write.format('jdbc').options(
        url='jdbc:postgresql://localhost:5432/model',
        dbtable='book_author',
        user='postgres',
        password='pass').mode('append').save()
