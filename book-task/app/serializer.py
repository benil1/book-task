from .models import AuthorModel
from .models import BooksModel
from .models import BookAuthorModel
from rest_framework import serializers




class AuthorSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = AuthorModel
        fields = ['average_rating', 'author_id','text_reviews_count','name','ratings_count']
        
class BookAuthorSerializer(serializers.HyperlinkedModelSerializer):
    book_title=serializers.SerializerMethodField('get_book_title')
    author_name=serializers.SerializerMethodField('get_author_name')
    class Meta:
        model = BookAuthorModel
        fields = ['book_title','author_name','book_id', 'author_id']
        
    def get_book_title(self,BookAuthorModel):
        title=BookAuthorModel.book.title
        return title
    
    def get_author_name(self,BookAuthorModel):
        name=BookAuthorModel.author.name
        return name

        
class BooksSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = BooksModel
        fields = ['book_id','title','title_without_series','publisher','description']
        
