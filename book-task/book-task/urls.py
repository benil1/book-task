from django.contrib import admin
from django.urls import include,path
from rest_framework import routers
from app import views 

urlpatterns = [
    path('api/author/',views.Author.as_view(),name = 'Authors'),
    path('api/books/',views.BookAuthor.as_view(),name = 'Books')
    
]
